set-face global InactiveCursor default,rgb:333333

# Reset to default colors when switching to an active client.
define-command unset-inactive-colors %{
  unset-face window PrimaryCursor
  unset-face window PrimaryCursorEol
  unset-face window SecondaryCursor
  unset-face window SecondaryCursorEol
  unset-face window LineNumberCursor
  unset-face window PrimarySelection
  unset-face window SecondarySelection
}

hook global FocusIn .* %{
  unset-inactive-colors
}

hook global WinDisplay .* %{
  unset-inactive-colors
}

# When we leave the client the color of the window changes to inactive colors
define-command set-inactive-colors %{
    set-face window PrimaryCursor InactiveCursor
    set-face window PrimaryCursorEol InactiveCursor
    set-face window SecondaryCursor InactiveCursor
    set-face window SecondaryCursorEol InactiveCursor
    set-face window LineNumberCursor LineNumbers
    set-face window PrimarySelection InactiveCursor
    set-face window SecondarySelection InactiveCursor
}

hook global FocusOut .* %{
  set-inactive-colors
}
