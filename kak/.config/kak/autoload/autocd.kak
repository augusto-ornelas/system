# Change the working directory based of the focused window
define-command set-working-directory %{
  evaluate-commands %sh{
    project="$($kak_config/autoload/find_project/find_project ${kak_buffile} 2> /dev/null)"
    if [ -n "$project" ]; then
      printf "set-option buffer wd %s\n" "%{$project}"
    else
      printf "set-option buffer wd %s\n" "%{$PWD}"
    fi
  }
}

hook global BufCreate .* %{
  set-working-directory
  hook buffer BufOpenFifo .* %{
    set-working-directory
  }
}

# Change automatically the working directory based on the current window
hook global WinDisplay .* %{ try %{ change-directory %opt{wd} }}
hook global FocusIn .* %{ try %{ change-directory %opt{wd} }}

define-command -file-completion -params 1 -docstring 'Update the current window''s working directory'  update-working-directory %{
  evaluate-commands %sh{
    printf "set-option buffer wd %s\n" "$(realpath "$1")"
  }
}

alias global ud update-working-directory
