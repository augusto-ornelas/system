declare-option -docstring 'The working directory of the runner buffer' str wd ''

define-command -hidden -override -params 1 -shell-completion \
    -docstring %{
        run [<arguments>]: run a shell command and put the output in a
        new buffer
     } run %{ evaluate-commands %sh{
     output=$(mktemp -d "${TMPDIR:-/tmp}"/kak-run.XXXXXXXX)/fifo
     mkfifo ${output}
     wd="$kak_opt_wd"
     command="$1"
     # printf %s\\n "$command" > ${output} 2>&1 & </dev/null
     ( eval "${command}" > ${output} 2>&1 & ) > /dev/null 2>&1 < /dev/null
     buff_name=$(echo "$command" | awk '{print $1}')
     printf %s\\n "evaluate-commands -try-client '$kak_opt_toolsclient' %{
               edit -fifo ${output} -scroll -- %{*run ${buff_name}*}
               set-option buffer filetype run
               hook -always -once buffer BufCloseFifo .* %{ nop %sh{ rm -r $(dirname ${output}) } }
           }"
}}

# TODO: Make this a history file and add it to the completions
declare-option -hidden str last_command ''

define-command -hidden run-prompt -shell-completion %{
  prompt -init %opt{last_command} -shell-completion 'command:' %{
    set-option global last_command %val{text}
    run %val{text}
  }
}

add-highlighter -override shared/run group
add-highlighter -override shared/run/ regex "((fatal )?\berror\b|\bfailed\b)|(\bwarning\b|\bignored\b)|(\bnote\b)|(required from(?: here)?))?.*?$" 1:red 2:red 3:yellow 4:blue 5:blue 6:yellow
add-highlighter -override shared/run/ regex "\bok\b|\bpassed\b" 0:green
add-highlighter -override shared/run/ regex "\bFAILED\b" 0:red

define-command -hidden -override run-jump %{
  evaluate-commands %{
    try %{
      execute-keys 'xs(.+):(\d+):(\d*)<ret>'
      evaluate-commands -verbatim -- edit -existing %reg{1} %reg{2} %reg{3} }
  }}

declare-user-mode comp-mode

hook global WinSetOption filetype=rust %{
  map window normal <c-g> ': enter-user-mode comp-mode<ret>'
  map window comp-mode t ': run %{cargo test}<ret>' -docstring 'cargo test'
  map window comp-mode c ': run %{cargo clippy}<ret>' -docstring 'cargo clippy'
}

hook global WinSetOption filetype=go %{
  map window normal <c-g> ': enter-user-mode comp-mode<ret>'
  map window comp-mode t ': run %{go test}<ret>' -docstring 'go test'
  map window comp-mode b ': run %{go build}<ret>' -docstring 'go build'
}

hook global WinSetOption filetype=c %{
  map window normal <c-g> ': enter-user-mode comp-mode<ret>'
  map window comp-mode t ': make test<ret>' -docstring 'make test'
  map window comp-mode b ': make<ret>' -docstring 'make'
}

  define-command -hidden run-bindings %{
  map window user n ': run-next-error<ret>' -docstring 'go to next error'
  map window user p ': run-prev-error<ret>' -docstring 'go to prev error'
}

define-command -hidden run-next-error %{
  evaluate-commands %{
    try %{
      execute-keys '/[\w/\.]+:\d+:\d*<ret>'
    } catch %{
      echo -markup {Error}No errors found{Default}
    }
  }
}

define-command -hidden run-prev-error %{
  evaluate-commands %{
    try %{
      execute-keys '<a-/>[\w/\.]+:\d+:\d*<ret>'
    } catch %{
      echo -markup {Error}No errors found{Default}
    }
}
}

hook global WinSetOption filetype=run %{
    hook buffer -group run-hooks NormalKey <ret> run-jump
    hook -once -always window WinSetOption filetype=.* %{ remove-hooks buffer run-hooks }
}

hook -group run-highlight global WinSetOption filetype=run %{
    add-highlighter window/run ref run
    run-bindings
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/run }
}

define-command -hidden -override mk -params .. %{ evaluate-commands %sh{
    wd=$PWD
    printf "%s\n" "make $@"
    printf "%s\n" "set-option window wd ${wd}"
}}

define-command -hidden -override gr -params .. %{ evaluate-commands %sh{
    wd=$PWD
    printf "%s\n" "grep $@"
    printf "%s\n" "set-option window wd ${wd}"
}}
