# Detection
hook global BufCreate .*\.scad$ %{
    set-option buffer filetype scad
}

hook global WinSetOption filetype=scad %[
    require-module scad
    set-option window comment_line "//"

    evaluate-commands "set-option window static_words %%opt{scad_static_words}"

    hook -group "scad-trim-indent" window ModeChange pop:insert:.* scad-trim-indent
    hook -group "scad-insert" window InsertChar \n scad-insert-on-newline
    hook -group "scad-indent" window InsertChar \n scad-indent-on-newline
    hook -group "scad-indent" window InsertChar \{ scad-indent-on-opening-curly-brace
    hook -group "scad-indent" window InsertChar \} scad-indent-on-closing-curly-brace

    hook -once -always window WinSetOption filetype=.* "
        remove-hooks window %val{hook_param_capture_1}-.+
    "
]

hook -group scad-highlight global WinSetOption filetype=scad %{
    add-highlighter window/scad ref scad
    hook -once -always window WinSetOption filetype=.* %{ remove-highlighter window/scad }
}

provide-module scad %§

define-command -hidden scad-trim-indent %{
    # remove the line if it's empty when leaving the insert mode
    try %{ execute-keys -draft <a-x> 1s^(\h+)$<ret> d }
}

define-command -hidden scad-indent-on-newline %< evaluate-commands -draft -itersel %<
    execute-keys <semicolon>
    try %<
        # if previous line is part of a comment, do nothing
        execute-keys -draft <a-?>/\*<ret> <a-K>^\h*[^/*\h]<ret>
    > catch %<
        # else if previous line closed a paren (possibly followed by words and a comment),
        # copy indent of the opening paren line
        execute-keys -draft k<a-x> 1s(\))(\h+\w+)*\h*(\;\h*)?(?://[^\n]+)?\n\z<ret> m<a-semicolon>J <a-S> 1<a-&>
    > catch %<
        # else indent new lines with the same level as the previous one
        execute-keys -draft K <a-&>
    >
    # remove previous empty lines resulting from the automatic indent
    try %< execute-keys -draft k <a-x> <a-k>^\h+$<ret> Hd >
    # indent after an opening brace or parenthesis at end of line
    try %< execute-keys -draft k <a-x> <a-k>[{(]\h*$<ret> j <a-gt> >
    # indent after a label
    try %< execute-keys -draft k <a-x> s[a-zA-Z0-9_-]+:\h*$<ret> j <a-gt> >
    # indent after a statement not followed by an opening brace
    try %< execute-keys -draft k <a-x> s\)\h*(?://[^\n]+)?\n\z<ret> \
                               <a-semicolon>mB <a-k>\A\b(if|for|while)\b<ret> <a-semicolon>j <a-gt> >
    try %< execute-keys -draft k <a-x> s \belse\b\h*(?://[^\n]+)?\n\z<ret> \
                               j <a-gt> >
    # deindent after a single line statement end
    try %< execute-keys -draft K <a-x> <a-k>\;\h*(//[^\n]+)?$<ret> \
                               K <a-x> s\)(\h+\w+)*\h*(//[^\n]+)?\n([^\n]*\n){2}\z<ret> \
                               MB <a-k>\A\b(if|for|while)\b<ret> <a-S>1<a-&> >
    try %< execute-keys -draft K <a-x> <a-k>\;\h*(//[^\n]+)?$<ret> \
                               K <a-x> s \belse\b\h*(?://[^\n]+)?\n([^\n]*\n){2}\z<ret> \
                               <a-S>1<a-&> >
    # deindent closing brace(s) when after cursor
    try %< execute-keys -draft <a-x> <a-k> ^\h*[})] <ret> gh / [})] <esc> m <a-S> 1<a-&> >
    # align to the opening parenthesis or opening brace (whichever is first)
    # on a previous line if its followed by text on the same line
    try %< evaluate-commands -draft %<
        # Go to opening parenthesis and opening brace, then select the most nested one
        try %< execute-keys [c [({],[)}] <ret> >
        # Validate selection and get first and last char
        execute-keys <a-k>\A[{(](\h*\S+)+\n<ret> <a-K>"(([^"]*"){2})*<ret> <a-K>'(([^']*'){2})*<ret> <a-:><a-semicolon>L <a-S>
        # Remove possibly incorrect indent from new line which was copied from previous line
        try %< execute-keys -draft <space> <a-h> s\h+<ret> d >
        # Now indent and align that new line with the opening parenthesis/brace
        execute-keys 1<a-&> &
     > >
> >

define-command -hidden scad-indent-on-opening-curly-brace %[
    # align indent with opening paren when { is entered on a new line after the closing paren
    try %[ execute-keys -draft -itersel h<a-F>)M <a-k> \A\(.*\)\h*\n\h*\{\z <ret> <a-S> 1<a-&> ]
    # align indent with opening paren when { is entered on a new line after the else
    try %[ execute-keys -draft -itersel hK <a-x> s \belse\b\h*(?://[^\n]+)?\n\h*\{<ret> <a-S> 1<a-&> ]
]

define-command -hidden scad-indent-on-closing-curly-brace %[
    # align to opening curly brace when alone on a line
    try %[
        # in case open curly brace follows a closing paren, align indent with opening paren
        execute-keys -itersel -draft <a-h><a-:><a-k>^\h+\}$<ret>hm <a-F>)M <a-k> \A\(.*\)\h\{.*\}\z <ret> <a-S>1<a-&>
    ] catch %[
        # otherwise align with open curly brace
        execute-keys -itersel -draft <a-h><a-:><a-k>^\h+\}$<ret>hm<a-S>1<a-&>
    ] catch %[]
]

define-command -hidden scad-insert-on-newline %[ evaluate-commands -itersel -draft %[
    execute-keys <semicolon>
    try %[
        evaluate-commands -draft -save-regs '/"' %[
            # copy the commenting prefix
            execute-keys -save-regs '' k <a-x>1s^\h*(//+\h*)<ret> y
            try %[
                # if the previous comment isn't empty, create a new one
                execute-keys <a-x><a-K>^\h*//+\h*$<ret> j<a-x>s^\h*<ret>P
            ] catch %[
                # if there is no text in the previous comment, remove it completely
                execute-keys d
            ]
        ]
    ]
    try %[
        # if the previous line isn't within a comment scope, break
        execute-keys -draft k<a-x> <a-k>^(\h*/\*|\h+\*(?!/))<ret>

        # find comment opening, validate it was not closed, and check its using star prefixes
        execute-keys -draft <a-?>/\*<ret><a-H> <a-K>\*/<ret> <a-k>\A\h*/\*([^\n]*\n\h*\*)*[^\n]*\n\h*.\z<ret>

        try %[
            # if the previous line is opening the comment, insert star preceeded by space
            execute-keys -draft k<a-x><a-k>^\h*/\*<ret>
            execute-keys -draft i*<space><esc>
        ] catch %[
           try %[
                # if the next line is a comment line insert a star
                execute-keys -draft j<a-x><a-k>^\h+\*<ret>
                execute-keys -draft i*<space><esc>
            ] catch %[
                try %[
                    # if the previous line is an empty comment line, close the comment scope
                    execute-keys -draft k<a-x><a-k>^\h+\*\h+$<ret> <a-x>1s\*(\h*)<ret>c/<esc>
                ] catch %[
                    # if the previous line is a non-empty comment line, add a star
                    execute-keys -draft i*<space><esc>
                ]
            ]
        ]

        # trim trailing whitespace on the previous line
        try %[ execute-keys -draft s\h+$<ret> d ]
        # align the new star with the previous one
        execute-keys K<a-x>1s^[^*]*(\*)<ret>&
    ]
] ]

evaluate-commands %sh{
        cat <<-EOF
            add-highlighter shared/scad regions
            add-highlighter shared/scad/code default-region group
            add-highlighter shared/scad/string region %{$maybe_at(?<!')(?<!'\\\\)"} %{(?<!\\\\)(?:\\\\\\\\)*"} fill string
            add-highlighter shared/scad/documentation_comment region /\*(\*[^/]|!) \*/ fill documentation
            add-highlighter shared/scad/line_documentation_comment region //[/!] $ fill documentation
            add-highlighter shared/scad/comment region /\\* \\*/ fill comment
            add-highlighter shared/scad/line_comment region // (?<!\\\\)(?=\\n) fill comment
            add-highlighter shared/scad/disabled region -recurse "#\\h*if(?:def)?" ^\\h*?#\\h*if\\h+(?:0|FALSE)\\b "#\\h*(?:else|elif|endif)" fill comment
            add-highlighter shared/scad/macro region %{^\\h*?\\K#} %{(?<!\\\\)(?=\\n)|(?=//)} group

            add-highlighter shared/scad/macro/ fill meta
            add-highlighter shared/scad/macro/ regex ^\\h*#\\h*include\\h+(\\S*) 1:module
            add-highlighter shared/scad/macro/ regex /\\*.*?\\*/ 0:comment
	EOF
}

add-highlighter shared/scad/code/numbers regex %{\b-?(0x[0-9a-fA-F]+|\d+)([uU][lL]{0,2}|[lL]{1,2}[uU]?|[fFdDiI]|([eE][-+]?\d+))?|'((\\.)?|[^'\\])'} 0:value
evaluate-commands %sh{
    # Grammar
    keywords='module function include use for if let intersection_for'
    consts='undef PI'
    builtin='circle square polygon text import projection sphere cube cylinder polyhedron linear_extrude rotate_extrude surface translate rotate scale resize mirror multmatrix color offset hull minkowski echo render children assert is_undef is_bool is_num is_string is_list is_function concat lookup str chr ord search version version_num parent_module abs sign sin cos tan acos asin atan atan2 floor round ceil ln len log pow sqrt exp rands min max norm cross union difference intersection'
    join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }

    # Add the language's grammar to the static completion list
    printf %s\\n "declare-option str-list scad_static_words $(join "${keywords} ${consts} ${builtin}" ' ')"

    # Highlight keywords
    printf %s "
        add-highlighter shared/scad/code/keywords regex \b($(join "${keywords}" '|'))\b 0:keyword
        add-highlighter shared/scad/code/builtin regex \b($(join "${builtin}" '|'))\b 0:builtin
        add-highlighter shared/scad/code/consts regex \b($(join "${const}" '|'))\b 0:value
    "
}

§
