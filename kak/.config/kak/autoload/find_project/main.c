#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#define DEBUG 0
typedef struct {
	char roots[5][30];
	char *file_extension;
	char *name;
} language;

enum { C, RUST, PYTHON, GO, JS, HTML, CSS, OTHER };

int filetype(char *filename)
{
	char *tok;
	tok = strtok(filename, ".");
	if (!tok) {
		return OTHER;
	}
	tok = strtok(0, ".");
	if (!tok) {
		return OTHER;
	}

	if (strcmp(tok, "js") == 0) {
		return JS;
	} else if (strcmp(tok, "rs") == 0) {
		return RUST;
	} else if (strcmp(tok, "c") == 0) {
		return C;
	} else if (strcmp(tok, "py") == 0) {
		return PYTHON;
	} else if (strcmp(tok, "go") == 0) {
		return GO;
	} else if (strcmp(tok, "html") == 0) {
		return HTML;
	} else if (strcmp(tok, "css") == 0) {
		return CSS;
	}

	return OTHER;
}

#define LANGS (7 + 1)

char *roots[][LANGS] = {
	{".git", "compile_commands.json", ".clangd", ".hg", ".project"},
	{"Cargo.toml", ".git", ".hg", ".project"},
	{"requirements.txt", "setup.py", ".git", ".hg", ".project"},
	{"Gopkg.toml", "go.mod", ".git", ".hg", ".project"},
	{"package.json", ".git", ".hg", ".project"},
	{"package.json", ".git", ".hg", ".project"},
	{"package.json", ".git", ".hg", ".project"},
	{".git", ".hg", ".project"},
};

int main(int argc, char **argv)
{
	DIR *p_dir;
	int language;
	struct dirent *p_dirent;
	char basedir[PATH_MAX], filename[PATH_MAX], realp[PATH_MAX];
	char *dir_name, *home_dir;
	struct stat statbuf;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <filepath>\n", argv[0]);
		return 1;
	}

	if (!(home_dir = getenv("HOME"))) {
		fprintf(stderr, "Could not find your $HOME enviroment, exiting");
		return 1;
	}

	strcpy(filename, argv[1]);

	if (lstat(filename, &statbuf) < 0) {
		if (errno == ENOENT) {
			fprintf(stderr, "Not a file\n");
			return 1;
		}
		fprintf(stderr, "Could not get stats: %s\n", strerror(errno));
		return 1;
	}

	if (DEBUG) {
		printf("filename %s\n", filename);
	}
	language = filetype(filename);
	if (!realpath(argv[1], realp)) {
		fprintf(stderr, "Could not resolve realative path: %s\n",
				strerror(errno));
		return 1;
	}
	dir_name = dirname(realp);
	if (DEBUG) {
		printf("dir_name %s\n", dir_name);
	}
	strcpy(basedir, dir_name);

	while (strcmp(dir_name, home_dir) != 0 && strcmp(dir_name, "/") != 0) {
		p_dir = opendir(dir_name);
		if (p_dir == NULL) {
			printf("Cannot open directory '%s'\n", dir_name);
			return 1;
		}
		while ((p_dirent = readdir(p_dir)) != NULL) {
			for (int i = 0; roots[language][i] != NULL && i < 5; ++i) {
				if (strcmp(roots[language][i], p_dirent->d_name) == 0) {
					if (DEBUG) {
						printf("Project directory: '%s'\n", dir_name);
						closedir(p_dir);
						printf("Found root: '%s'\n", p_dirent->d_name);
					} else {
						printf("%s\n", dir_name);
					}
					return 0;
				}
			}
		}
		dir_name = dirname(dir_name);
		closedir(p_dir);
	}
	printf("%s\n", basedir);
	return 0;
}
