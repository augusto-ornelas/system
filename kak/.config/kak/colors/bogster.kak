# bogster theme from helix editor
evaluate-commands %sh{
    background="rgb:161c23"
    foreground="rgb:d5ded6"
    bright_fore="rgb:dddddd"
    dim_foreground="rgb:bbbbbb"
    whitespace="rgb:374758"
    menu_background="rgb:232d38"
    menu_foreground="rgb:6EE7BA"
    menu_info="rgb:59c0dc"
    primary_sel_back="rgb:313f4e"
    secondary_sel_back="rgb:3E342B"
    matching_char_foreground="rgb:dc7759"
    status_cursor="rgb:69B5AE"
    current_word="rgb:6F8884"
    comments="rgb:828ec9"
    linum="rgb:475a71"
    type="rgb:dc597f"
    secondary_cursor="rgb:6E7A91"
    primary_cursor="$dim_foreground"
    values="rgb:59c0dc"
    string="rgb:59dcb7"
    prompt="$menu_foreground"
    keyword="rgb:dcb659"
    functions="rgb:59dcd8"
    module="rgb:d32c5d"
    links="rgb:3EBA83"
    error="rgb:dc597f"
    title="rgb:98E6BA"
    warnings="rgb:dc7759"

    echo "
         set-face global value $values
         set-face global type $type
         set-face global variable $dim_foreground
         set-face global function $functions
         set-face global module $module+b
         set-face global string $string
         set-face global error $error
         set-face global keyword $keyword+b
         set-face global operator $warnings
         set-face global attribute $keyword+b
         set-face global comment $comments+i
         set-face global meta $keyword+b
         set-face global builtin $keyword

         set-face global title $title+b
         set-face global header $values+b
         set-face global mono $dim_foreground
         set-face global block $string
         set-face global link $links
         set-face global bullet $type
         set-face global list $menu_info
         set-face global documentation magenta

         set-face global Default $foreground,$background

         set-face global PrimarySelection default,$primary_sel_back+g
         set-face global PrimaryCursor $background,$primary_cursor+gf
         set-face global PrimaryCursorEol $background,$primary_cursor

         set-face global SecondarySelection default,$secondary_sel_back+g
         set-face global SecondaryCursor $foreground,$secondary_cursor+gf
         set-face global SecondaryCursorEol $background,$secondary_cursor

         set-face global MatchingChar $matching_char_foreground,$background+bF
         set-face global Search $status_cursor,$string
         set-face global CurrentWord $foreground,$current_word

         # listchars
         set-face global Whitespace $whitespace,$background+f
         # ~ lines at EOB now nice
         set-face global BufferPadding $whitespace,$background
         # must use wrap -marker hl
         set-face global WrapMarker Whitespace

         set-face global LineNumbers $linum,$background
         # must use -hl-cursor
         set-face global LineNumberCursor $primary_cursor,$background
         set-face global LineNumbersWrapped $whitespace,$background

         # complement in autocomplete like path
         set-face global MenuInfo $menu_info,default
         # when item focused in menu
         set-face global MenuForeground $bright_fore,$whitespace+b
         # default bottom menu and autocomplete
         set-face global MenuBackground $bright_fore,$menu_background
         # clippy
         set-face global Information default,$menu_background
         set-face global Error $background,$error

         # all status line: what we type, but also client@[session]
         set-face global StatusLine $foreground,$background
         # insert mode, prompt mode
         set-face global StatusLineMode $background,$prompt
         # message like '1 sel'
         set-face global StatusLineInfo $prompt,$background
         # count
         set-face global StatusLineValue $keyword,$background
         set-face global StatusCursor $background,$status_cursor
         # like the word 'select:' when pressing 's'
         set-face global Prompt $background,$string
    "
}
