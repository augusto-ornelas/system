##
## reeder theme
## a light theme inspired after https://github.com/hyspace/st2-reeder-theme
##

evaluate-commands %sh{
    white="rgb:f9f8f6"
    white_light="rgb:f6f5f0"
    black="rgb:383838"
    black_light="rgb:635240"
    grey_dark="rgb:c6b0a4"
    grey_light="rgb:e8e8e8"
    grey_mid="rgb:B3B3B3"
    brown_dark="rgb:af4609"
    brown_light="rgb:baa188"
    brown_lighter="rgb:f0e7df"
    orange="rgb:fc7302"
    orange_light="rgb:f88e3b"
    green="rgb:438047"
    green_light="rgb:7ba84d"
    red="rgb:f03c3c"

    # Base color definitions
    echo "
        # then we map them to code
        set-face global value         ${orange_light}+b
        set-face global type          ${orange}
        set-face global variable      default
        set-face global module        ${green}
        set-face global function      default
        set-face global string        ${green}
        set-face global keyword       ${brown_dark}
        set-face global operator      default
        set-face global attribute     ${green}
        set-face global comment       ${brown_light}
        set-face global documentation comment
        set-face global meta          ${brown_dark}
        set-face global builtin       default+b

        # and markup
        set-face global title      ${orange}+b
        set-face global header     ${orange}+b
        set-face global mono       ${green_light}
        set-face global block      ${green}
        set-face global link       ${orange}
        set-face global bullet     ${brown_dark}
        set-face global list       ${black}

        # and built in faces
        set-face global Default            ${black_light},${white}
        set-face global PrimarySelection   ${black},${grey_mid}+fg
        set-face global PrimaryCursor      ${white},${black}+fg
        set-face global PrimaryCursorEol   ${white},${black}+fg
        set-face global SecondarySelection ${black_light},${brown_light}+fg
        set-face global SecondaryCursor    ${black},${brown_dark}+fg
        set-face global SecondaryCursorEol ${black},${brown_dark}+fg
        set-face global LineNumbers        ${grey_dark},${white}
        set-face global LineNumberCursor   ${black},${white}
        set-face global LineNumbersWrapped +i@Whitespace
        set-face global MenuForeground     ${orange},${brown_lighter}
        set-face global MenuBackground     ${black_light},${brown_lighter}
        set-face global MenuInfo           ${black},default
        set-face global Information        ${black_light},${brown_lighter}
        set-face global Error              default,${red}
        set-face global DiagnosticError    ${red}
        set-face global DiagnosticWarning  ${orange}
        set-face global StatusLine         ${black},${white}
        set-face global StatusLineMode     ${white},${orange_light}
        set-face global StatusLineInfo     ${green},${white}
        set-face global StatusLineValue    ${green_light}
        set-face global StatusCursor       ${white_light},${orange}
        set-face global Prompt             ${black_light},${white}
        set-face global MatchingChar       default+b
        set-face global BufferPadding      ${grey_dark},${white}
        set-face global Whitespace         ${grey_dark}+f
    "
}
