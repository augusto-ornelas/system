# night sky theme

# 81.9 % #14354C Blue
# 3.3  % #98E6BA Green
# 3.0  % #6596A7 Blue
# 2.5  % #69B5AE Blue
# 2.4  % #359171 Green
# 2.3  % #6EE7BA Green
# 1.8  % #105952 Green
# 1.4  % #3EBA83 Green
# 0.9  % #6F8884 Grey
# 0.4  % #2CD895 Green
# 0.1  % #F3C33F Yellow
# 0.01 % #F36464 Red

evaluate-commands %sh{
    background="rgb:071118"
    foreground="rgb:E6E1CF"
    dim_foreground="rgb:DDDDDD"
    whitespace="rgb:105952"
    menu_background="rgb:062320"
    menu_foreground="rgb:98E6BA"
    menu_info="rgb:359171"
    primary_sel_back="rgb:1B4968"
    secondary_sel_back="rgb:384542"
    matching_char_back="rgb:344D65"
    status_cursor="rgb:69B5AE"
    linum="rgb:6F8884"
    current_word="rgb:6F8884"
    comments="rgb:F8DE95"
    secondary_cursor="rgb:336662"
    primary_cursor="rgb:5EA4D4"
    values="rgb:9FBEC8"
    string="rgb:2CD895"
    links="rgb:3EBA83"
    keyword="rgb:69B5AE"
    functions="rgb:3EBA83"
    error="rgb:F36464"
    title="rgb:98E6BA"
    warnings="rgb:F3C33F"

    echo "
         set-face global value $values
         set-face global type $keyword
         set-face global variable $foreground
         set-face global function $functions
         set-face global module $dim_foreground
         set-face global string $string
         set-face global error $error
         set-face global keyword $keyword
         set-face global operator $string
         set-face global attribute $keyword
         set-face global comment $comments+i
         set-face global meta $functions
         set-face global builtin $foreground+b

         set-face global title $title
         set-face global header $keyword
         set-face global mono $links
         set-face global block $values
         set-face global link $links
         set-face global bullet $links
         set-face global list $foreground

         set-face global Default $foreground,$background

         set-face global PrimarySelection default,$primary_sel_back+ga
         set-face global PrimaryCursor $background,$primary_cursor+F
         set-face global PrimaryCursorEol $background,$primary_cursor

         set-face global SecondarySelection default,$secondary_sel_back+ga
         set-face global SecondaryCursor $foreground,$secondary_cursor+F
         set-face global SecondaryCursorEol $background,$secondary_cursor

         set-face global MatchingChar $foreground,$matching_char_back
         set-face global Search $status_cursor,$string
         set-face global CurrentWord $foreground,$current_word

         # listchars
         set-face global Whitespace $whitespace,$background+f
         # ~ lines at EOB now nice
         set-face global BufferPadding $whitespace,$background
         # must use wrap -marker hl
         set-face global WrapMarker Whitespace

         set-face global LineNumbers $linum,$background
         # must use -hl-cursor
         set-face global LineNumberCursor $primary_cursor,$background+b
         set-face global LineNumbersWrapped $whitespace,$background+i

         # complement in autocomplete like path
         set-face global MenuInfo $menu_info,default
         # when item focused in menu
         set-face global MenuForeground $background,$menu_foreground+b
         # default bottom menu and autocomplete
         set-face global MenuBackground $dim_foreground,$menu_background
         # clippy
         set-face global Information default,$menu_background
         set-face global Error $background,$error

         # all status line: what we type, but also client@[session]
         set-face global StatusLine $foreground,$background
         # insert mode, prompt mode
         set-face global StatusLineMode $background,$string
         # message like '1 sel'
         set-face global StatusLineInfo $string,$background
         # count
         set-face global StatusLineValue $keyword,$background
         set-face global StatusCursor $background,$status_cursor
         # like the word 'select:' when pressing 's'
         set-face global Prompt $background,$string
    "
}
