# ayu theme
# https://github.com/ayu-theme/ayu-colors

evaluate-commands %sh{
    black="rgb:0A0E14"
    gray="rgb:273747"
    gray1="rgb:1B2733"
    gray2="rgb:4D5566"
    gray3="rgb:8B95A7"
    gray4="rgb:678AAC"
    gray5="rgb:344D65"
    white="rgb:E6E1CF"
    dim_white="rgb:DDDDDD"
    pink="rgb:F07178"
    dark_red="rgb:310221"
    brown="rgb:E6B673"
    light_yellow="rgb:FFEE99"
    blue="rgb:59C2FF"
    cyan="rgb:39BAE6"
    mint="rgb:95E6CB"
    green="rgb:C2D94C"
    yellow="rgb:E6B460"
    orange="rgb:FF8740"
    bright_orange="rgb:FFB454"
    red="rgb:FF3333"
    title="rgb:FFB454"

    echo "
         set-face global value $cyan
         set-face global type $orange
         set-face global variable $white
         set-face global function $bright_orange
         set-face global module $dim_white
         set-face global string $mint
         set-face global error $red
         set-face global keyword $orange
         set-face global operator $mint
         set-face global attribute $orange
         set-face global comment $light_yellow+i
         set-face global meta $bright_orange
         set-face global builtin $white+b

         set-face global title $title
         set-face global header $orange
         set-face global mono $green
         set-face global block $cyan
         set-face global link $green
         set-face global bullet $green
         set-face global list $white

         set-face global Default $white,$black

         set-face global PrimarySelection default,$gray5+g
         set-face global PrimaryCursor $black,$cyan+F
         set-face global PrimaryCursorEol $black,$cyan

         set-face global SecondarySelection default,$gray2+g
         set-face global SecondaryCursor $black,$light_yellow+F
         set-face global SecondaryCursorEol $black,$light_yellow

         set-face global MatchingChar $white,$gray5
         set-face global Search $white,$mint
         set-face global CurrentWord $white,$gray2

         # listchars
         set-face global Whitespace $gray,$black+f
         # ~ lines at EOB now nice
         set-face global BufferPadding $gray,$black
         # must use wrap -marker hl
         set-face global WrapMarker Whitespace

         set-face global LineNumbers $gray3,$black
         # must use -hl-cursor
         set-face global LineNumberCursor $cyan,$black+b
         set-face global LineNumbersWrapped $gray,$black+i

         # complement in autocomplete like path
         set-face global MenuInfo $gray2,default
         # when item focused in menu
         set-face global MenuForeground $black,$yellow+b
         # default bottom menu and autocomplete
         set-face global MenuBackground $dim_white,$gray1
         # clippy
         set-face global Information default,$gray1
         set-face global Error $black,$pink

         # all status line: what we type, but also client@[session]
         set-face global StatusLine $white,$black
         # insert mode, prompt mode
         set-face global StatusLineMode $black,$mint
         # message like '1 sel'
         set-face global StatusLineInfo $mint,$black
         # count
         set-face global StatusLineValue $orange,$black
         set-face global StatusCursor $black,$blue
         # like the word 'select:' when pressing 's'
         set-face global Prompt $black,$mint
    "
}
