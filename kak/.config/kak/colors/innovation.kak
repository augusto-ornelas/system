# night sky theme

#26AAC4 Blue
#6596A7 Blue
#69B5AE Blue
#2CD895 Green
#6EE7BA Green
#11DBB4 Green
#105952 Green
#6F8884 Grey
#D4C995 Yellow
#D3AE2C Yellow
#E0248C Purple
#F0A4FF Purple
#DE4582 Red
#E5EAE6 White
#C8D3CC Dim white
#ED9980 Salmon
# menu_foreground="rgb:F3F5D7"

evaluate-commands %sh{
    background="rgb:191B1C"
    foreground="rgb:E5EAE6"
    dim_foreground="rgb:C8D3CC"
    whitespace="rgb:363A3C"
    menu_background="rgb:232527"
    menu_foreground="rgb:6EE7BA"
    menu_info="rgb:F0A4FF"
    primary_sel_back="rgb:163B53"
    secondary_sel_back="rgb:384542"
    matching_char_back="rgb:344D65"
    status_cursor="rgb:69B5AE"
    linum="rgb:5C6468"
    current_word="rgb:6F8884"
    comments="rgb:949CA0"
    secondary_cursor="rgb:336662"
    primary_cursor="rgb:5EA4D4"
    values="rgb:ED9980"
    string="rgb:2CD895"
    prompt="$menu_foreground"
    keyword="rgb:E0248C"
    functions="rgb:26AAC4"
    module="rgb:F0A4FF"
    links="rgb:3EBA83"
    error="rgb:DE4582"
    title="rgb:98E6BA"
    warnings="rgb:F3C33F"

    echo "
         set-face global value $values
         set-face global type $keyword
         set-face global variable $foreground
         set-face global function $functions
         set-face global module $module
         set-face global string $string
         set-face global error $error
         set-face global keyword $keyword
         set-face global operator $string
         set-face global attribute $keyword
         set-face global comment $comments+i
         set-face global meta $functions
         set-face global builtin $module

         set-face global title $title
         set-face global header $keyword
         set-face global mono $links
         set-face global block $values
         set-face global link $links
         set-face global bullet $links
         set-face global list $foreground

         set-face global Default $foreground,$background

         set-face global PrimarySelection default,$primary_sel_back+ga
         set-face global PrimaryCursor $background,$primary_cursor+F
         set-face global PrimaryCursorEol $background,$primary_cursor

         set-face global SecondarySelection default,$secondary_sel_back+ga
         set-face global SecondaryCursor $foreground,$secondary_cursor+F
         set-face global SecondaryCursorEol $background,$secondary_cursor

         set-face global MatchingChar $foreground,$matching_char_back+bF
         set-face global Search $status_cursor,$string
         set-face global CurrentWord $foreground,$current_word

         # listchars
         set-face global Whitespace $whitespace,$background+f
         # ~ lines at EOB now nice
         set-face global BufferPadding $whitespace,$background
         # must use wrap -marker hl
         set-face global WrapMarker Whitespace

         set-face global LineNumbers $linum,$background
         # must use -hl-cursor
         set-face global LineNumberCursor $primary_cursor,$background+b
         set-face global LineNumbersWrapped $whitespace,$background+i

         # complement in autocomplete like path
         set-face global MenuInfo $menu_info,default
         # when item focused in menu
         set-face global MenuForeground $background,$menu_foreground+b
         # default bottom menu and autocomplete
         set-face global MenuBackground $dim_foreground,$menu_background
         # clippy
         set-face global Information default,$menu_background
         set-face global Error $background,$error

         # all status line: what we type, but also client@[session]
         set-face global StatusLine $foreground,$background
         # insert mode, prompt mode
         set-face global StatusLineMode $background,$prompt
         # message like '1 sel'
         set-face global StatusLineInfo $prompt,$background
         # count
         set-face global StatusLineValue $keyword,$background
         set-face global StatusCursor $background,$status_cursor
         # like the word 'select:' when pressing 's'
         set-face global Prompt $background,$string
    "
}
