#!/bin/sh

swaymsg -t subscribe -m '[ "window" ]' | jq --unbuffered --compact-output 'if(.container.inhibit_idle) then 5 else 5 end' | xargs -I% pkill -RTMIN+% waybar
