#!/bin/sh
wifi_icon="$HOME/Pictures/wifi-icon.png"

notify-send -i "$wifi_icon" "Loking for available networks"

if [ $(which nmcli) ]; then
	# Create a list of available networks, pass it to dmenu to choose
	# one and save it
	ssid=$(nmcli -f SSID device wifi list | sed "1 d" | menu.sh -l 5 -p "Choose a wifi network")

	# Remove the extra spaces
	ssid=$(echo $ssid | sed -e 's/^[[:space:]]*//')

	if [ -n "${ssid}" ]; then
		alacritty --class 'Alacritty Popup' -t "Wifi Password" -e sh -c "nmcli device wifi connect '${ssid}' -a"
	fi
else
	interface=$(wpa_cli scan | awk '/Selected/ {print $3}' | tr -d "'")
	sleep 1
	ssid=$(wpa_cli -i $interface scan_results | sed '1d' | cut -f5 | menu.sh -l 5 -p 'Select the wifi network')
	id=$(wpa_cli -i $interface add_network)
	wpa_cli -i $interface set_network $id ssid '"'$ssid'"'
	psk=$(echo "" | menu.sh -p "'$ssid' password:")
	[ -n "$ssid" ] && wpa_cli -i $interface set_network $id psk '"'$psk'"' || exit 1
	[ -n "$ssid" ] && wpa_cli -i $interface enable "$ssid" && wpa_cli -i $interface save_config && notify-send -i $wifi_icon "Connected to $ssid"
fi
