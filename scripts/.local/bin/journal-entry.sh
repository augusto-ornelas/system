#!/bin/sh

JOURNAL_PATH="$HOME/Documents/journal"
now=$(date -Is)
today=$(date -u --date="$now" '+%F')
entry=$(printf '%s\n' start stop | menu.sh -p entry -l 2)

if [ $entry = 'start' ]; then
	((tail -n 1 < "$JOURNAL_PATH/$today") 2> /dev/null | grep start) > /dev/null && echo "${now}: stop" >> "$JOURNAL_PATH/$today"
	# TODO: handle the case of 'No project'
	project="$(ultralist ls group:project | grep -E '^[a-zA-Z]' | menu.sh -p 'Choose a project:' -l 5)"
	task="$(ultralist ls project:${project} | grep -E '^[0-9]' | menu.sh -p 'Choose a tast:' -l 5 | cut -c 33-)"
	description="${project} | ${task}"
fi


# TODO: detect when the last event didn't finish yet and stop it
echo "${now} | $entry | $description" >> "$JOURNAL_PATH/$today"


