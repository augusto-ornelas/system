#!/bin/bash

# Install Go for all users

apt-get -y install golang

for DIR in $(ls -1d /root /home/* 2>/dev/null || true)
do
	cat <<-'EOF'>> $DIR/.bashrc
	GOPATH=$HOME/go
	PATH=$PATH:$GOPATH/bin
	EOF

	# Fix rights
	USR=$(echo "$DIR" | rev | cut -d "/" -f1  | rev)
	chown -R $USR:$USR $DIR
done
