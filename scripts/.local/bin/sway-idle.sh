#!/bin/sh

swaymsg -t subscribe -m '[ "window", "tick" ]' | jq --unbuffered --compact-output 'if(.container.inhibit_idle == true or .payload == "activated") then {text: "", class: "activated", alt: "activated"} else {text: "", class: "deactivated", alt: "deactivated"} end'
