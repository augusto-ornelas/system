#!/bin/sh

state=$(cat "$(find /tmp -maxdepth 1 -name 'pomodoro.*')")

hours=$(printf "%s / 3600\n" "$state" | bc)
min=$(printf "%s %% 3600 / 60\n" "$state" | bc)
sec=$(printf "%s %% 60\n" "$state" | bc)

printf "%02d:%02d:%02d\n" "$hours" "$min" "$sec"
