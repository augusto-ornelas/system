cdp() {
	if [ -n "$1" ]; then
		cd "$1" && echo "$(basename "$1"):  $PWD" >> ~/.config/kak/.projects
	else
		cd "$(sk --height=80% < "$HOME/.config/kak/.projects" | cut -d' ' -f3)"
	fi
}
