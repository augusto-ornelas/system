#!/bin/sh

powered=$(bluetoothctl show | grep Powered: | cut -d' ' -f 2)

if [ "$powered" != 'yes' ]; then
	bluetoothctl power on
fi

device="$(bluetoothctl devices Paired | cut -d' ' -f 2- | menu.sh -l 5 -p 'Connect to: ' | cut -d' ' -f 1| xargs)"

if [ "$device" ]; then
	bluetoothctl connect "$device"
fi

