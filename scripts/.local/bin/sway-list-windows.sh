#!/bin/sh

swaymsg -t get_tree | jq -r '[recurse(.nodes[]?)|recurse(.floating_nodes[]?)|select(.type == "workspace")|{workspace: .name, window: (recurse(.nodes[]?)|recurse(.floating_nodes[]?))} | select(.window.focused|not)| select(.window.type="con"),select(.window.type=="floating_con") | select(.window.pid!=null) | { workspace, id: .window.id, name: .window.name, type: .window.type, app_id: (if(.window.app_id!=null)then .window.app_id else .window.window_properties.class end)}]|unique |.[] | "💻 " + .workspace + " | "+ (.id|tostring) + ": " + .name + " @"+.app_id'
