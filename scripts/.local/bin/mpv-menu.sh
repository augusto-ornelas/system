#!/bin/sh

video=$(fd '.*\.(mp4|mkv)' . | menu.sh -p 'Choose a video' -l 5)

[ -n "$video" ] && mpv "$video"
