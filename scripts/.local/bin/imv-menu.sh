#!/bin/sh

# Instead of doing this just list the directories with at least one picture inside
image_dir="$(fd '.*\.(jpeg|png|jpg)' | xargs -d'\n' dirname | sort | uniq | menu.sh -p 'image dir > ')"

[ -n "$image_dir" ] && imv "$image_dir"
