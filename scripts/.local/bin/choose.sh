#!/bin/sh

file="$1"
shift
menu.sh "$@" -p "Select a glyph: " < "$file" | cut -d' ' -f1 | wl-copy -n
