#!/bin/sh

op="$1"

[ -n "$op" ] || op=$(echo -e "mount\nunmount" | menu.sh -l 2 -p 'operation > ')

[ -z "$op" ] && exit 1

device="$(lsblk -J -o NAME,MODEL,PATH,HOTPLUG,TYPE,SIZE,FSTYPE | jq -r '.blockdevices[] | select(.hotplug) | .path + " " + .vendor + .model + " " + .size + " " + .fstype' | menu.sh -p "$op > " | cut -d' ' -f1 | xargs)"

[ -z "$device" ] && exit 1

case $op in
	mount)
		partition="$(lsblk -J -o NAME,MODEL,PATH,HOTPLUG,TYPE,SIZE,FSTYPE,LABEL,MOUNTPOINT "$device" | jq -r '.blockdevices[].children[] | select(.hotplug and .mountpoint == null) | .path + " " + .label + " " +  .size + " " + .fstype' | menu.sh -p "$op > " | cut -d' ' -f1 | xargs)"
		;;
	unmount)
		partition="$(lsblk -J -o NAME,MODEL,PATH,HOTPLUG,TYPE,SIZE,FSTYPE,LABEL,MOUNTPOINT "$device" | jq -r '.blockdevices[].children[] | select(.hotplug and .mountpoint != null) | .path + " " + .label + " " +  .size + " " + .fstype' | menu.sh -p "$op > " | cut -d' ' -f1 | xargs)"
		;;
	*)
		exit 1
		;;
esac

[ -z "$partition" ] && exit 1

dir="$(udisksctl $op -b "$partition" | cut -d' ' -f4 )"

[ -z "$dir" ] && exit 1

alacritty --working-directory="$dir"
