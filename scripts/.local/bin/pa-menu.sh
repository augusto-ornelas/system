#!/bin/sh

set_default()
{
	pactl -f json list "${1}s" | jq -r '.[] | select(.ports[].availability == "available") | (.index|tostring)+" "+(if .mute then "🔇" else "🔊" end)+" "+.description' | menu.sh -l 5 -p "Default $1:" | cut -d' ' -f1 | xargs -I% echo pactl "set-default-$1" %
}

what=$(echo -e "source\nsink" | menu.sh -l 5 -p "Set:")

[ -n "$what" ] && set_default $what
