#!/bin/sh

#Icons made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

wifi_icon=~/Pictures/wifi-icon.png

notify-send -t 1000 -i $wifi_icon "Looking for available networks"

if [ "$(command -v nmcli)" ]; then
	ssid="$(nmcli -f NAME connection show | sed "1d" | menu.sh -l 5 -p 'Select the wifi network' | xargs)"
	ssid="$(echo "$ssid" | sed -e 's/^[[:space:]]*//')"
	[ -n "$ssid" ] && nmcli connection up "$ssid" && notify-send -i $wifi_icon "Connected to $ssid"
else
	netid="$(wpa_cli list_networks | sed '1,2d' | cut -f1,2 | menu.sh -l 5 -p 'Select network: ' | cut -f1)"
	[ -n "$netid" ] && wpa_cli select_network "$netid" && notify-send -i $wifi_icon "Connected to $netid"
fi

