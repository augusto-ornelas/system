#!/bin/sh

sink=$(pactl list short | grep RUNNING | cut -f1 | head -n 1)

if [ -z "$sink" ]; then
	sink=@DEFAULT_SINK@
fi

case "$1" in
	up)
		pactl set-sink-volume "$sink" "+$2" && notify-send -t 500 "🔊 $(pactl get-sink-volume $sink | grep Volume | cut -d'/' -f2 | tr -d ' ')"
	;;
	down)
		pactl set-sink-volume "$sink" "-$2" && notify-send -t 500 "🔉 $(pactl get-sink-volume $sink | grep Volume | cut -d'/' -f2 | tr -d ' ')"
		;;
	mute)
		pactl set-sink-mute "$sink" toggle
		pactl get-sink-mute "$sink" | grep no && notify-send -t 500 "🔊 $(pactl get-sink-volume $sink | grep Volume | cut -d'/' -f2 | tr -d ' ')" || notify-send -t 500 '🔇'
		;;
	*)
		notify-send "Wrong option: volume-control up|down|mute"
		;;
esac
