#!/bin/sh

swaymsg -t subscribe -m '["window"]' | jq --unbuffered 'select(.change == "focus") | if .container.window_rect.width > .container.window_rect.height then "splith" else "splitv" end' | xargs -I% swaymsg %
