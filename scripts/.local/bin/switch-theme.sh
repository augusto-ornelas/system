#!/bin/sh

case $1 in dark)
			sed -i 's/colors: \*light/colors: \*dark/' $HOME/system/alacritty/.config/alacritty/alacritty.yml
			swaymsg "output * bg ~/Pictures/wallpaper fill"
			swaymsg '[tiling]' border pixel 1
			swaymsg default_border pixel 1
			# change the background image and the sway colors
			swaymsg client.focused          "#cccccc #222222 #ffffff #eeeeee   #aaaaaa"
			swaymsg client.focused_inactive "#333333 #000000 #ffffff #484e50   #333333"
			swaymsg client.unfocused        "#333333 #222222 #888888 #292d2e   #333333"
			swaymsg client.urgent           "#2f343a #660022 #ffffff #660022   #660022"
			swaymsg client.placeholder      "#000000 #0c0c0c #ffffff #000000   #0c0c0c"
			kak_clients=$(kak -l)
			for client in $kak_clients; do
				echo 'colorscheme innovation' | kak -p "$client"
				# echo "client $client"
			done
			# change the gtk theme
			gsettings set org.gnome.desktop.interface gtk-theme 'Seventeen-Dark-Solid'
			rm -r $HOME/system/waybar/.config/waybar/style.css
			ln -s $HOME/system/waybar/.config/waybar/style-dark.css $HOME/system/waybar/.config/waybar/style.css
			pkill waybar
			waybar -b bar-0 &
			sed -i 's/colorscheme reeder/colorscheme innovation/' $HOME/system/kak/.config/kak/kakrc
			# create an alternative theme for waybar
			;;
		light)
			sed -i 's/colors: \*dark/colors: \*light/' $HOME/system/alacritty/.config/alacritty/alacritty.yml
			swaymsg "output * bg ~/Pictures/monstain-snow.jpg fill"
			swaymsg '[tiling]' border pixel 2
			swaymsg default_border pixel 2
			swaymsg client.focused          "#279a74 #ffffff #222222 #279a74   #279a74"
			swaymsg client.focused_inactive "#cccccc #222222 #ffffff #eeeeee   #dddddd"
			swaymsg client.unfocused        "#cccccc #222222 #ffffff #eeeeee   #dddddd"
			swaymsg client.urgent           "#2f343a #660022 #ffffff #660022   #660022"
			swaymsg client.placeholder      "#000000 #0c0c0c #ffffff #000000   #0c0c0c"
			kak_clients=$(kak -l)
			for client in $kak_clients; do
				echo 'colorscheme reeder' | kak -p "$client"
				# echo "client $client"
			done
			# create an alternative theme for waybar
			gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Lighter-solid'
			rm -r $HOME/system/waybar/.config/waybar/style.css
			ln -s $HOME/system/waybar/.config/waybar/style-light.css $HOME/system/waybar/.config/waybar/style.css
			pkill waybar
			waybar -b bar-0 &
			sed -i 's/colorscheme innovation/colorscheme reeder/' $HOME/system/kak/.config/kak/kakrc
			;;
		 *)
			echo 'Nothing to do'
			;;
esac


