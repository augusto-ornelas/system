#!/bin/sh 

player=$(playerctl -l | dmenu -i -p "Choose the player to stop")


if [ ! -z $player ]; then
	playerctl -p $player play-pause
fi
