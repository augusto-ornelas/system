#!/usr/bin/env python3

import getopt
import sys
import os
import logging
from i3ipc import Connection, Event

logging.basicConfig(level=logging.DEBUG)


def find_parent(i3_con, window_id):
    """
        Find the parent of a given window id
    """
    def finder(con, parent):
        if con.id == window_id:
            return parent
        for node in con.nodes:
            res = finder(node, con)
            if res:
                return res
        return None
    return finder(i3_con.get_tree(), None)


def set_layout(self, event):
    """
        Set the layout/split for the currently
        focused window to either vertical or
        horizontal, depending on its width/height
    """
    win = event.container
    parent = find_parent(self, win.id)

    if (parent and parent.layout != 'tabbed'
            and parent.layout != 'stacked'):
        win = self.get_tree().find_focused()
        if 1.8*win.rect.height > win.rect.width:
            self.command('splitv')
        else:
            self.command('splith')


def print_help():
    """
    Print the help of the of the script
    """
    print("Usage: " + sys.argv[0] + " [-p path/to/pid.file]")
    print("")
    print("Options:")
    print("    -p path/to/pid.file   Saves the PID for this program in the",
          "filename specified")
    print("")


def main():
    """
    Main function - listen for window focus
        changes and call set_layout when focus
        changes
    """
    opt_list, _ = getopt.getopt(sys.argv[1:], 'hp:')
    pid_file = None
    for opt in opt_list:
        if opt[0] == "-h":
            print_help()
            sys.exit()
        if opt[0] == "-p":
            pid_file = opt[1]

    if pid_file:
        with open(pid_file, 'w') as the_file:
            the_file.write(str(os.getpid()))

    i3_con = Connection()
    i3_con.on(Event.WINDOW_FOCUS, set_layout)
    i3_con.main()


if __name__ == "__main__":
    main()
