#!/bin/sh

set -x

fname="${XDG_RUNTIME_DIR}/wlr-randr.last"

all_out="$(wlr-randr | awk '/^[^[:space:]]/ {print $1}')"

turn_on_all_outputs() {
	for o in $all_out; do
		wlr-randr --output "$o" --on
	done
}

case "$1" in
--restore)
	[ "$(echo "$all_out" | wc -l)" = 1 ] && wlr-randr --output eDP-1 --on && exit 0
	if [ -e "$fname" ]; then
		while read -r output state; do
			wlr-randr --output "$output" "$state" || turn_on_all_outputs
		done < "$fname"
	else
		turn_on_all_outputs
	fi
	;;
--on|--off)
	wlr-randr | awk '/^[^[:space:]]/,/Enabled/ { if(match($0, /^[^[:space:]]/)!=0){output = $1} else if($1 == "Enabled:") {sub(/yes/, "--on");sub(/no/, "--off"); print output, $2}}' > "$fname"
	for o in $all_out; do
		wlr-randr --output "$o" "$1"
	done
	;;
*)
	echo "'$1' is not a valid option"
	echo "Valid options are: --restore, --on, --off"
	;;
esac

