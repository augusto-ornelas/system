#!/bin/bash

wind_name() {
	# swaymsg -t get_tree | jq --raw-output 'recurse(.nodes[]) | select(.focused) | .name'
	swaymsg -t subscribe -m "['window']" | jq --unbuffered -r .container.name
}

## TEMP
temp() {
	temp=$(cat /sys/class/thermal/thermal_zone1/temp)
	temp=$(( temp / 1000 ))
	echo -e "$temp "
}

## RAM
mem() {
  mem=$(free | awk '/Mem/ {printf "%d\n", $3 / $2 * 100 }')
  echo -e "$mem%"
}

## CPU
cpu() {
  read -r cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read -r cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo -e "$cpu%"
}

## Status bar config
get_battery() {
	local state perc icon
	perc=$(cat /sys/class/power_supply/BAT0/capacity)
	state=$(cat /sys/class/power_supply/BAT0/status)

    if [[ "$state" = "Charging" ]]; then
      icon=" "
    elif (( perc < 10 )); then
      icon=""
    elif (( perc == 100 )) || [[ "$state" = "fully-charged" ]]; then
      icon=""
    else
      case "$(echo "$perc" | cut -c1)" in
          9) icon="";;
        7|8) icon="";;
        5|6) icon="";;
        3|4) icon="";;
        1|2) icon="";;
      esac
    fi

    echo -n "$perc% $icon"
}

get_wifi() {
	wpa_cli status | grep "^ssid" | cut -d= -f2
}

get_status() {
	echo -n "$(get_wifi)     $(cpu)     $(mem)     $(temp)     $(get_battery)    $(date '+%H:%M:%S %F') "
}

while :
do
	get_status
	sleep 0.5
done

swaymsg -t subscribe -m "['window']" | jq --unbuffered '.container.name' | xargs -I'{}' printf '<b>%s<\\b>\t\t\t%s\n' '{}' "$(get_status)"
