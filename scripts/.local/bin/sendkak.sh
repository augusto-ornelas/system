#!/bin/sh

sendkak() {
	while test $# -ge 2; do
	case "$1" in
		"-s") server="$2";;
		"-c") client="$2";;
		*)
			echo "Error: not recognize flag" $1
			exit 1
		;;
	esac
	shift 2
	done

	if [ -f $1 ]; then
		filename="$(realpath $1)"
		printf "eval -client client${client} %%{ edit %s }\n" "$filename" | kak -p $server && swaymsg "[title=\".*client${client}@\[${server}\].*\"] focus"
	else
		echo "Not such file or directory $1"
		exit 1
	fi
}

