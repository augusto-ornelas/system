#!/bin/sh

# Choose between predefined monitor layouts
laptop='eDP-1'

case "$XDG_CURRENT_DESKTOP" in
	sway)
		selected_setup=$(printf "%s\n" work home zen | menu.sh -p 'screen layout:' -l 5)
		external_monitor=$(swaymsg -r -t get_outputs | jq '.[].name' | sed "/${laptop}/d")
		number_of_external_monitors=$(echo $external_monitor | wc -w)
		if [ $number_of_external_monitors -ne 1 ]; then
			notify-send "unknown setup"
			exit 1
		fi
		case "$selected_setup" in
			zen)
				swaymsg output "$laptop" disable
				;;
			work)
		# This is my work set up an external monitor on top of the laptop
				swaymsg output "$external_monitor" pos 0 0 res 2560x1440 enable \
				&& swaymsg output "$laptop" pos 0 1440 res 1920x1080 enable
				;;
			home)
		# This is my home set up: an external monitor left to my laptop
				swaymsg output "$external_monitor" pos 0 0 res 2560x1440 enable \
				&& swaymsg output "$laptop" pos 2560 0 res 1920x1080 enable
				;;
			*)
				notify-send "Nothing to do"
				;;
		esac
		;;
	dwl)
		selected_setup=$(printf '%s\n' home zen | menu.sh -p 'screen layout:' -l 5)
		external_monitor=$(wlr-randr | grep '^[^[:space:]]' | sed "/${laptop}/d" | awk '{print $1}')
		number_of_external_monitors=$(echo $external_monitor | wc -w)
		if [ $number_of_external_monitors -ne 1 ]; then
			notify-send "unknown setup"
			exit 1
		fi
		case "$selected_setup" in
			zen)
				wlr-randr --output "$laptop" --off
				;;
			home)
				wlr-randr --output "$laptop" --on && sleep 1 && wlr-randr --output "$laptop" --pos 0,0 --output "$laptop" --pos 2560,0
				;;
			*)
				notify-send "Nothing to do"
				;;
		esac
		;;
esac
