#!/bin/sh

active_interfaces="$(route | sed -r '1,2d; s/ +/ /g' | cut -d' ' -f8 | sort -u)"

if [ -z "$active_interfaces" ]; then
	printf "%s\n" " Disconnected"
elif echo "$active_interfaces" | grep '^en' > /dev/null 2>&1; then
	printf "%s\n" 
else
	wpa_state="$(wpa_cli status | awk '/^wpa_state=/ {split($0, a, "="); print a[2]}')"
	if [ "$wpa_state" = "DISCONNECTED" ]; then
		printf "%s\n" " Disconnected"
	elif [ "$wpa_state" = "COMPLETED" ]; then
		printf "%s(%s%%) \n" "$(wpa_cli status | awk '/^ssid=/ {split($0,a,"="); print a[2]}')" "$(printf "define min(a,b) {\nif(a < b)\nreturn a\nreturn b\n}\nmin(((%s + 85) * 5)/2, 100)\n" "$(iw dev wlp3s0 station dump | awk '/^\s+signal avg:/ {print $3}')" | bc)"
	else
		echo "${wpa_state} "
	fi
fi

