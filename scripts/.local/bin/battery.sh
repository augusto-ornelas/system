#!/bin/sh

capacity=$(upower -i $(upower -e | grep -i bat0) | grep percentage | tr -s ' ' | sed 's/^[[:space:]]*//' | cut -d ' ' -f2 | sed 's/%//')
echo $capacity
