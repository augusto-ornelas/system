#!/bin/sh

dbus-update-activation-environment --all &

gentoo-pipewire-launcher restart &

swaybg -i "${HOME}/Pictures/wallpaper" -m fill -o '*' &

waybar -c "${HOME}/Documents/src/dwl/waybar/config" -s "${HOME}/Documents/src/dwl/waybar/style.css" &

mako &

swayidle -w \
	timeout 270 'notify-send -i ~/Pictures/warning-sign.png -u low "LOCKING in 30 secs."' \
	timeout 300 'playerctl -a pause ; swaylock -f -i ~/Pictures/wallpaper' \
	timeout 600 'all-outputs.sh --off' \
	resume 'all-outputs.sh --restore' \
	timeout 1200 'loginctl suspend' \
	before-sleep 'swaylock -f -i ~/Pictures/wallpaper' \
	after-resume 'all-outputs.sh --restore' &

gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita' &
gsettings set org.gnome.desktop.interface icon-theme 'Adwaita' &
gsettings set org.gnome.desktop.interface cursor-theme 'Adwaita' &
gsettings set org.gnome.desktop.interface font-name 'IBM Plex Sans,  13' &
