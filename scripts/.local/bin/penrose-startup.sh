#!/bin/sh

# set up the monitors in the right position
autorandr --change > /dev/null

# fix a couple of quirks with my thinkpad: enable tap-click for the touchpad
# and slow down the track point accelleration
xinput --set-prop "11" "libinput Tapping Enabled" 1
xinput --set-prop "12" "libinput Accel Speed" 0.0

# set the background
feh --bg-fill ~/Pictures/wallpaper &

running() { pgrep -fi "$1" >/dev/null; }

running sxhkd || sxhkd &

running emacs || emacs --bg-daemon

running xautolock || xautolock \
						 -detectsleep \
						 -time 3 \
						 -locker "i3lock -k -c 555555" \
						 -killtime 10 \
						 -killer "systemctl suspend" \
						 -notify 30 \
						 -notifier "notify-send -i ~/Pictures/warning-sign.png -u critical -t 120 -- 'LOCKING screen in 30 seconds...'" &

running dunst || dunst &

running batsignal || batsignal &

$HOME/.config/scripts/status-bar.sh &

