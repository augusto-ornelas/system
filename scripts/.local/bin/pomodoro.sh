#!/bin/sh

trap cleanup 0 1 2 3 15

cleanup() {
	rm -f "$state"
	exit 1
}

state=$(mktemp /tmp/pomodoro.XXXXXX)
notify-send -i ~/Pictures/wall-clock.png 'Pomodoro timer started'
i=1500
while [ "$i" -gt 0 ]; do
	echo "$i" > "$state"
	sleep 1
	i=$(echo "$i - 1" | bc)
done
notify-send -i ~/Pictures/wall-clock.png 'Time to take a break' && aplay -d 1 ~/Downloads/mixkit-alert-alarm-1005.wav > /dev/null 2>&1
