#!/bin/bash

POLL=2
## TEMP
temp() {
	temp=`sensors | grep edge | awk '{ print $2 }'`
	echo -e "$temp"
}

## RAM
mem() {
  mem=`free | awk '/Mem/ {printf "%d\n", $3 / $2 * 100 }'`
  echo -e "$mem%"
}

## CPU
cpu() {
  read cpu a b c previdle rest < /proc/stat
  prevtotal=$((a+b+c+previdle))
  sleep 0.5
  read cpu a b c idle rest < /proc/stat
  total=$((a+b+c+idle))
  cpu=$((100*( (total-prevtotal) - (idle-previdle) ) / (total-prevtotal) ))
  echo -e " $cpu%"
}

# Status bar config
get_battery() {
	local state perc icon
	read -r state perc <<< $(upower -i "/org/freedesktop/UPower/devices/battery_BAT0" |
      awk '/state|percentage/ { print $2 }' |
      xargs |
      tr -d '%')

    if [[ "$state" = "charging" ]]; then
      icon=""
    elif (( perc < 10 )); then
      icon=""
    elif (( perc == 100 )) || [[ "$state" = "fully-charged" ]]; then
      icon=""
    else
      case "$(echo "$perc" | cut -c1)" in
          9) icon="";;
        7|8) icon="";;
        5|6) icon="";;
        3|4) icon="";;
        1|2) icon="";;
      esac
    fi

    echo -n "$perc% $icon  "
}

get_wifi() {
	nmcli -g con | grep " connected" | awk '{print $4}'
}

get_status() {
	echo -n " $(cpu)    $(mem)    $(temp)    $(get_wifi)    $(get_battery)   $(date '+%F %R')"
}

while true; do
  new_status="$(get_status)"
  [ "$current_status" = "$new_status" ] || xsetroot -name "$new_status"
  current_status="$new_status"
  sleep $POLL
done
