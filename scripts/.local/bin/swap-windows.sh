#!/bin/sh

swaymsg -- mark --add current

id=$(sway-list-windows.sh | menu.sh -l 5 -p 'swap with:' | awk '{sub(/:/,""); print $4 }')

swaymsg swap container with con_id $id
swaymsg [con_mark=current] focus
swaymsg -- mark --toggle current
