baobab
chromium-browser
dconf-editor
firefox
gnome-calculator
gnome-system-monitor
gnome-tweaks
mpv
curl -sL https://deb.nodesource.com/setup_11.x | bash -
apt-get -y install nodejs

apt-get -y install apt-utils autossh bash-completion binutils build-essential console-data console-setup cron curl dialog dmidecode fail2ban file git iftop ifupdown iotop iptables iputils-ping isc-dhcp-client isc-dhcp-common jq less linux-headers-generic linux-image-generic locales lshw lsof lzop man mc moreutils nano net-tools ntpdate pciutils psmisc python python3 rsync rsyslog ssh sudo sysstat telnet tig traceroute tree tzdata unzip usbutils wget zip
