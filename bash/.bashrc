# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
	*i*) ;;
	  *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=2000
HISTFILESIZE=10000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
	xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
	if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
	else
	color_prompt=
	fi
fi

if [ "$color_prompt" = yes ]; then
	PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
	PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
	PS1="\\[\\e]0;${debian_chroot:+($debian_chroot)}\\u@\\h: \\w\\a\\]$PS1"
	;;
*)
	;;
esac

case "$TERM" in
xterm*|rxvt*|alacritty)
	 PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
	;;
*)
	;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	#alias dir='dir --color=auto'
	#alias vdir='vdir --color=auto'

	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
	alias sch='schedule -f ~/Documents/c/schedule/work-log.schedule'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias calc='rpn_calc'
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
	. /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
  fi
fi

export GOPATH=$HOME/go
export PATH=$HOME/.cargo/bin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$HOME/.local/bin:$GOPATH/bin
export PYTHON_VENV=$HOME/.local/share/venvs/bin
export PATH="$PATH:$PYTHON_VENV"

# get current branch in git repo
function parse_git_branch() {
	BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=$(parse_git_dirty)
		echo "  ${BRANCH}${STAT}"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=$(git status 2>&1 | tee)
	dirty=$(echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?")
	untracked=$(echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?")
	ahead=$(echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?")
	newfile=$(echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?")
	renamed=$(echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?")
	deleted=$(echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?")
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits="≍${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="≻${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="∄${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="-${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="±${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

if [ -f "$HOME/.local/bin/skimgit.sh" ]; then
	. "$HOME/.local/bin/skimgit.sh"
fi

if [ -f "$HOME/.local/bin/util.sh" ]; then
	. "$HOME/.local/bin/util.sh"
fi

if [ -f "$HOME/.config/sk/key-bindings.bash" ]; then
	. "$HOME/.config/sk/key-bindings.bash"
fi

if [ -f "$HOME/.config/nnn/quitcd.bash_zsh" ]; then
	. "$HOME/.config/nnn/quitcd.bash_zsh"
fi

bold="\[$(tput bold)\]"
blue="\[$(tput setaf 4)\]"
reset="\[$(tput sgr0)\]"

export PS1="┌── $bold$blue\w$reset\$(parse_git_branch)
└─➤ "

export GPG_TTY=$(tty)
export GTK_IM_MODULE=ibus gedit
export XMODIFIERS="@im=ibus" xterm
export QT_IM_MODULE=ibus kwrite
export VISUAL=kak
export EDITOR=$VISUAL
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MOZ_ENABLE_WAYLAND=1
export QT_QPA_PLATFORMTHEME="qt5ct"
export SKIM_DEFAULT_OPTIONS="--bind='alt-k:preview-up,alt-p:preview-up ' --bind='alt-j:preview-down,alt-n:preview-down' --reverse"
if command -v  fd &> /dev/null; then
	export SKIM_DEFAULT_COMMAND="fd -Ht f ."
fi
if command -v bemenu &> /dev/null; then
	export BEMENU_OPTS='-m focused --fn "LiberationSans 15" --tb "#DDDDDD" --tf "#222222" --fb "#222222" --ff "#DDDDDD" --nb "#222222" --nf "#DDDDDD" --hb "#DDDDDD" --hf "#222222" --sb "#222222" --sf "#DDDDDD"'
fi

# Get color in man pages
export MANPAGER="less -R --use-color -Ds+y -DP+wK -Dd+c -Du+b"
export MANROFFOPT="-c"
# -----------------------
export BAT_THEME="ansi"
export TERMINAL=alacritty
export TPM2_PKCS11_STORE=/etc/tpm2_pkcs11
complete -cf doas
